import argparse
from json import load
from os import mkdir, path

from pybower import install


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument(
        '--dest',
        default='bower_components',
        help='destination folder (default: ./bower_components)')
    parser.add_argument(
        'package',
        nargs='*',
        help='packages to install instead of ones listed in bower.json')
    parser.add_argument('--verbose', '-v', action='count')
    parser.add_argument(
        '--bowerrc',
        default='bower.json',
        help='path to bower.json (default: ./bower.json)')
    args = parser.parse_args()
    if args.package:
        deps = args.package
    else:
        # read bower.json
        deps = []
        with open(args.bowerrc) as rc:
            rc = load(rc)
            deps += rc['dependencies']
    # install every dep in deps
    if not path.exists(args.dest):
        mkdir(args.dest)
    for dep in deps:
        depdir = path.join(args.dest, dep)
        if not path.exists(depdir):
            install(args.dest, dep, verbosity=args.verbose)


if __name__ == '__main__':
    main()
