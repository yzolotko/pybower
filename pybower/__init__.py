from __future__ import print_function
from semver_range import Version, Range

from fnmatch import fnmatch
from json import load
from os import mkdir, path
from shutil import copy2, rmtree
from tempfile import mkdtemp
from git import Repo, Git
import requests

try:
    from urllib import basejoin
except ImportError:
    from urllib.parse import urljoin as basejoin


def install(basedir, pkgname, ver=None, verbosity=0):
    pkgdir = path.join(basedir, pkgname)
    if verbosity:
        print('Installing', pkgname, ver or '', 'to', pkgdir)
    # search bower registry
    r = requests.get(basejoin('https://registry.bower.io/packages/',
                              pkgname)).json()
    # clone repository to temporary directory
    tmpdir = mkdtemp()
    if verbosity:
        print('Cloning', r['url'], 'to', tmpdir)
    # https://stackoverflow.com/questions/35965157/how-to-git-clone-a-specific-tag-only-without-getting-the-whole-repo
    # https://stackoverflow.com/questions/20734181/how-to-get-list-of-latest-tags-in-remote-git#20742976
    tags = Git().ls_remote('-t', r['url'])
    tags = [line.split('refs/tags/')[-1] for line in tags.splitlines()]
    versions = []
    for tag in tags:
        try:
            v = tag.lstrip('v')
            versions.append(Version(str(v)))
        except ValueError as e:
            pass
    best = Range(ver or '*').highest_version(versions)
    if verbosity:
        print('Highest matching version is %s' % best)
    for tag in tags:
        if tag.lstrip('v') == str(best):
            if verbosity:
                print('Cloning tag %s' % tag)
            repo = Repo.clone_from(r['url'], tmpdir, depth=1, branch=tag)
            break
    bower = path.join(tmpdir, 'bower.json')
    if path.exists(bower):
        with open(bower) as info:
            info = load(info)
    else:
        info = {}
    # install dependencies
    for dep, ver in info.get('dependencies', {}).items():
        depdir = path.join(basedir, dep)
        if not path.exists(depdir):
            if verbosity:
                print('Installing', dep, ver, 'required by', pkgname)
            install(basedir, dep, ver)
    # copy package files from repository
    ignore = info.get('ignore', [])
    copytree(repo.head.commit.tree, pkgdir, ignore)
    rmtree(tmpdir)
    if verbosity:
        print('Successfully installed', pkgname)


def copytree(tree, dest, ignore):
    for i in ignore:
        if fnmatch(tree.path, i):
            return
    if tree.type == 'tree':
        mkdir(path.join(dest, tree.path))
        for t in tree:
            copytree(t, dest, ignore)
    else:
        copy2(tree.abspath, path.join(dest, tree.path))
