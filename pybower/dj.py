from __future__ import print_function

from json import load
from os import mkdir, path
from threading import Lock

from django.conf import settings
from django.contrib.staticfiles.finders import BaseStorageFinder
from django.core.files.storage import FileSystemStorage

from . import install

_lock = Lock()


def get_bower_components_path():
    return path.join(settings.BASE_DIR, 'bower_components')


def get_bowerrc():
    return path.join(settings.BASE_DIR, 'bower.json')


class BowerComponentFinder(BaseStorageFinder):
    storage = FileSystemStorage(get_bower_components_path())

    def install_components(self):
        with _lock:
            deps = {
                x: None
                for x in getattr(settings, 'BOWER_INSTALLED_APPS', [])
            }
            # read bower.json
            rc = get_bowerrc()
            if path.exists(rc):
                with open(rc) as rc:
                    rc = load(rc)
                    deps.update(rc['dependencies'])
            # install every dep in deps
            base = get_bower_components_path()
            if not path.exists(base):
                mkdir(base)
            for dep, ver in deps.items():
                depdir = path.join(base, dep)
                if not path.exists(depdir):
                    install(base, dep, ver, 1)

    def find(self, path, all=False):
        self.install_components()
        return super(BowerComponentFinder, self).find(path, all)

    def list(self, ignore_patterns):
        self.install_components()
        return super(BowerComponentFinder, self).list(ignore_patterns)
