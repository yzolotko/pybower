# pybower

pybower installs packages from bower repository.

# Usage

* Install pybower package:
```shell
pip install git+https://bitbucket.org/yzolotko/pybower.git
```
* Install packages from `bower.json` to `bower_components`
```shell
pybower
```
* OR Install `jquery` to `bower_components`
```shell
pybower jquery
```

# Django integration

pybower provides static file finder which automatically installs components from bower.json when running Django's collectstatic. Unlike `django-collectstatic-bower` or `django-bower`, this application does not require bower executable or nodejs to be installed which makes it suitable to be run in environments like Heroku.

Usage:

* Install pybower package:
```shell
pip install git+https://bitbucket.org/yzolotko/pybower.git
```
* Add it to `STATICFILES_FINDERS`:
```python
STATICFILES_FINDERS = (
        ...
        'pybower.dj.BowerComponentFinder',
        )
```
* Create bower.json if it does not exist yet.
```shell
bower init
bower install jquery --save # for example
```
* OR specify required packages in settings.py:
```python
BOWER_INSTALLED_APPS = (
    'jquery',
    'underscore',
    'foundation-sites',
)
```
* Reference static files in your template:
```html
    <link rel="stylesheet" href="/static/foundation-sites/dist/css/foundation.css" />
```
* When running `manage.py collectstatic` missing packages from bower.json will be downloaded and copied to `STATIC_ROOT`.
